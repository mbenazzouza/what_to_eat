import { Recipe } from "./recipe";


export const RECIPES: Recipe[] = [
     { id: 1, name: 'Quinoa-Chickpea-Sweet Potato Bowls with Peanut Sauce', url: 'https://bit.ly/3F5lrcd'},
     { id: 2, name: 'Air-Fried Green Tomatoes', url: 'https://bit.ly/37SH1og'},
     { id: 3, name: 'Creamy Dairy-Free Dill Ranch Dip', url: 'https://bit.ly/376Eg20'},
     { id: 4, name: 'The Best Broccoli Slaw', url: 'https://bit.ly/3ORjwfP'},
     { id: 5, name: 'Beet Greens and Spinach Saag with Tofu', url: 'https://bit.ly/3vzRX2U'},
     { id: 6, name: 'Stir-Fry Wraps with Peanut Dipping Sauce', url: 'https://bit.ly/3ORgFU7'},

]